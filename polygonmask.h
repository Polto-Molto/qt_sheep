#ifndef POLYGONMASK_H
#define POLYGONMASK_H

#include <QWidget>
#include <QPolygon>
#include <settingsvalues.h>
#include <QSqlDatabase>
#include <opencv2/opencv.hpp>

using namespace cv;

namespace Ui {
class polygonmask;
}

class polygonmask : public QWidget
{
    Q_OBJECT

public:
    explicit polygonmask(Camera*,QSqlDatabase*,QWidget *parent = 0);
    ~polygonmask();
    QString image_file;
    QPolygon p1,p2,p3,p4,p5;
    int currentPolygon;
    Camera *cam;
    QSqlDatabase* db;
    Mat image ;


public slots:
    void renderToImage();
    void setCurrentPolygon(int);
    void clear();
    void clearAll();
    void undo();

protected:
    void mouseReleaseEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *);

private:
    Ui::polygonmask *ui;
};

#endif // POLYGONMASK_H
