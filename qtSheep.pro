#-------------------------------------------------
#
# Project created by QtCreator 2016-10-30T22:07:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qtSheep
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    video_widget.cpp \
    video_thread.cpp

HEADERS  += widget.h \
    video_widget.h \
    video_thread.h

FORMS    += widget.ui
