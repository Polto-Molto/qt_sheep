#include "video_widget.h"
#include "video_thread.h"
#include <QPainter>

video_widget::video_widget(QWidget *parent) :
    QWidget(parent)
{
    frame.create(Size(320,240),CV_8UC3);
}

void video_widget::initwidget(video_thread *vid)
{
    this->setMinimumSize(vid->vid_width,vid->vid_height);
    this->setMaximumSize(vid->vid_width,vid->vid_height);
    this->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);

    frame.create(Size(vid->vid_width,vid->vid_height),CV_8UC3);
    connect(vid , SIGNAL(commingFrame(Mat*)) , this ,SLOT(getNewFrame(Mat*)));

}

void video_widget::getNewFrame(Mat* newframe)
{
    cvtColor(*newframe , frame,CV_BGR2RGB);
    this->update();
}

void video_widget::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    QImage tImg((const uchar*)frame.data, frame.cols, frame.rows , QImage::Format_RGB888);
    p.drawImage(0,0,tImg);
}
