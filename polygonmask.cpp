#include "polygonmask.h"
#include "ui_polygonmask.h"
#include <QPainter>
#include <QMouseEvent>
#include <QDebug>
polygonmask::polygonmask(Camera* c,QSqlDatabase* rdb,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::polygonmask)
{
    cam = c;
    db =rdb;
    ui->setupUi(this);
    image_file = cam->RecordPath+"/images/setting.png";
    image = imread(image_file.toUtf8().data());
    //Size size(120,100);
    //cv::resize(image,image,size);
    ui->setupUi(this);
    currentPolygon = 1;
}

polygonmask::~polygonmask()
{
    delete ui;
}

void polygonmask::setCurrentPolygon(int p)
{
    currentPolygon = p;
    update();
}

void polygonmask::undo()
{
    switch (currentPolygon) {
    case 1:
        if(!p1.isEmpty())
            p1.pop_back();
        break;

    case 2:
        if(!p2.isEmpty())
            p2.pop_back();
        break;

    case 3:
        if(!p3.isEmpty())
            p3.pop_back();
        break;

    case 4:
        if(!p4.isEmpty())
            p4.pop_back();
        break;

    case 5:
        if(!p5.isEmpty())
            p5.pop_back();
        break;
    }
    this->update();
}

void polygonmask::clearAll()
{
    p1.clear();
    p2.clear();
    p3.clear();
    p4.clear();
    p5.clear();
}

void polygonmask::clear()
{
    switch (currentPolygon) {
    case 1:
        p1.clear();
        break;

    case 2:
        p2.clear();
        break;

    case 3:
        p3.clear();
        break;

    case 4:
        p4.clear();
        break;

    case 5:
        p5.clear();
        break;
    }
    this->update();
}

void polygonmask::paintEvent(QPaintEvent *event)
{
        QPainter p(this);
     //   QImage tImg((const uchar*)image.data, image.cols, image.rows  , QImage::Format_RGB888);
     //   p.drawImage(0,0,tImg);

        QPen pen(Qt::red,5,Qt::SolidLine);

            pen.setColor(Qt::red);
            pen.setWidth(4);
            if(currentPolygon == 1)
                pen.setWidth(8);
            p.setPen(pen);
            p.drawPolygon(p1);

            pen.setColor(Qt::blue);
            pen.setWidth(4);
            if(currentPolygon == 2)
                pen.setWidth(8);
            p.setPen(pen);
            p.drawPolygon(p2);

            pen.setColor(Qt::yellow);
            pen.setWidth(4);
            if(currentPolygon == 3)
                pen.setWidth(8);
            p.setPen(pen);
            p.drawPolygon(p3);

            pen.setColor(Qt::green);
            pen.setWidth(4);
            if(currentPolygon == 4)
                pen.setWidth(8);
            p.setPen(pen);
            p.drawPolygon(p4);
            pen.setColor(QColor(254, 98, 0));
            pen.setWidth(4);
            if(currentPolygon == 5)
                pen.setWidth(8);
            p.setPen(pen);
            p.drawPolygon(p5);

}

// mouse pressed make error
void  polygonmask::mouseReleaseEvent(QMouseEvent *event)
{
    switch (currentPolygon) {
    case 1:
        p1 << event->pos();
        break;

    case 2:
        p2 << event->pos();
        break;

    case 3:
        p3 << event->pos();
        break;

    case 4:
        p4 << event->pos();
        break;

    case 5:
        p5 << event->pos();
        break;
    }
    this->update();
}

void polygonmask::renderToImage()
{
    QImage bitmap(this->size(),QImage::Format_RGB888);
    bitmap.fill(Qt::transparent);

    QPainter p(&bitmap);

    QBrush brush;
    brush.setColor(Qt::white);
    brush.setStyle(Qt::SolidPattern);

    QPainterPath path;
    path.addPolygon(p1);
    path.addPolygon(p2);
    path.addPolygon(p3);
    path.addPolygon(p4);
    path.addPolygon(p5);
    p.fillPath(path,brush);
    qDebug() << p1;
    //this->render(&p,QPoint(),QRegion(),QWidget::DrawChildren);
    bitmap.save(image_file+".png");
}
