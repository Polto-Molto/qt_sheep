#include "video_thread.h"
#include <QDebug>
#include <opencv/cv.h>

video_thread::video_thread(QObject *parent) :
    QThread(parent)
{

    fini = true;
}

video_thread::~video_thread()
{
    mutex.lock();
    frame.release();
    cap.release();
    mutex.unlock();
}

void video_thread::fin()
{
    fini = false;
}

void video_thread::setVideoFile(QString fn)
{
    mutex.lock();
    cap.open(fn.toUtf8().data());
    mutex.unlock();

}

void video_thread::run()
{
    Mat capframe;
    while(cap.isOpened())
    {
        if(!fini) break;
       mutex.lock();
        cap >> capframe;
        resize(capframe , frame  , frame.size() , 0,0,INTER_LINEAR);
        Q_EMIT(commingFrame(&frame));
        msleep(50);
       mutex.unlock();
    }
    capframe.release();
    exit(0);
}
