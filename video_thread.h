#ifndef VIDEO_THREAD_H
#define VIDEO_THREAD_H

#include <QThread>
#include <QMutex>
#include "opencv/highgui.h"

using namespace cv;

class video_thread : public QThread
{
    Q_OBJECT
public:
    explicit video_thread(QObject *parent = 0);
    ~video_thread();

    QMutex mutex;
    void setVideoFile(QString);
    void run();

public Q_SLOTS:
    void fin();

signals:
    void commingFrame(Mat* );


public:
    VideoCapture cap;
    Mat frame;
    bool fini;

};

#endif // VIDEO_THREAD_H
