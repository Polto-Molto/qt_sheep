#ifndef VIDEO_WIDGET_H
#define VIDEO_WIDGET_H

#include <QWidget>
#include "opencv/cv.h"
#include "video_thread.h"

using namespace cv;

class video_widget : public QWidget
{
    Q_OBJECT
public:
    explicit video_widget(QWidget *parent = 0);
    Mat frame;
    void initwidget(video_thread *);

public slots:
    void getNewFrame(Mat*);

protected:
    void paintEvent(QPaintEvent *);

};

#endif // VIDEO_WIDGET_H
